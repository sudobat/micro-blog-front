document.addEventListener("DOMContentLoaded", () => {
    console.log('Microblog v1');

    // Common functionallity
    const responseToJson = function(response) {
        return response.json();
    };

    const acceptJsonHeaders = new Headers({
        'Accept': 'application/json'
    });

    // Articles API
    const articleContainer = document.getElementById('blog-article');
    
    const getArticleRequest = new Request('http://localhost/api/v1/articles/8', {
        method: 'GET',
        mode: 'cors',
        headers: acceptJsonHeaders
    });

    const fillArticleContainer = function(article) {
        console.log('Article', article);

        const articleTitleContainer = document.createElement('h1');
        articleTitleContainer.innerHTML = article.title;

        const articleContentContainer = document.createElement('div');
        articleContentContainer.innerHTML = article.content;

        // Clear previous Article
        while (articleContainer.lastChild) {
            articleContainer.removeChild(articleContainer.lastChild);
        }

        // Add Article
        articleContainer.appendChild(articleTitleContainer);
        articleContainer.appendChild(articleContentContainer);
    };

    const fallbackArticle = function(error) {
        console.log('article error', error);
    };

    fetch(getArticleRequest)
        .then(responseToJson)
        .then(fillArticleContainer)
        .catch(fallbackArticle);

    // Comments API
    const commentsContainer = document.getElementById('blog-comments');
    const commentsForm = document.getElementById('blog-comments-form');

    const getCommentsRequest = new Request('http://localhost/api/v1/comments', {
        method: 'GET',
        mode: 'cors',
        headers: acceptJsonHeaders
    });

    const createCommentRequest = function() {
        return new Request('http://localhost/api/v1/comments', {
            method: 'POST',
            body: new FormData(commentsForm),
            mode: 'cors',
            headers: acceptJsonHeaders
        });
    };

    const fillCommentsContainer = function(comments) {
        console.log('Comments', comments);

        // Clear previous Comments
        while (commentsContainer.lastChild) {
            commentsContainer.removeChild(commentsContainer.lastChild);
        }

        // Add all the Comments
        comments.forEach(comment => {
            const commentContainer = document.createElement('div');
            commentContainer.setAttribute('class', 'blog-article-comment');
            
            const commentContent = document.createElement('p');
            commentContent.innerHTML = comment.content;

            commentContainer.appendChild(commentContent);

            commentsContainer.appendChild(commentContainer);
        });
    };

    const fallbackComments = function(error) {
        console.log('comments error', error);
    };

    const fallbackCommentSubmit = function(error) {
        console.log('comment submit error', error);
    }

    fetch(getCommentsRequest)
        .then(responseToJson)
        .then(fillCommentsContainer)
        .catch(fallbackComments);

    commentsForm.addEventListener('submit', function(e) {
        e.preventDefault();

        fetch(createCommentRequest())
            .then(responseToJson)
            .then(function(newComment) {
                console.log('comment submit response', newComment);

                // Create Comment HTML
                const commentContainer = document.createElement('div');
                commentContainer.setAttribute('class', 'blog-article-comment');
                
                const commentContent = document.createElement('p');
                commentContent.innerHTML = newComment.content;

                // Prepend new Comment
                commentsContainer.insertBefore(commentContent, commentsContainer.firstChild);

                // Clean current Comment Form
                commentsForm.reset();
            })
            .catch(fallbackCommentSubmit);
    });

    // Users API
    let currentUser = null;

    const registerForm = document.getElementById('blog-register-form');

    const loginForm = document.getElementById('blog-login-form');

    const usersContainer = document.getElementById('blog-users');

    const userInfoContainer = document.getElementById('blog-user-info');

    const userSignUpButton = document.getElementById('blog-user-sign-up-button');

    const getUsersRequest = new Request('http://localhost/api/v1/users', {
        method: 'GET',
        mode: 'cors',
        headers: new Headers({
            'Accept': 'application/json'
        })
    });

    const createUserRequest = function() {
        return new Request('http://localhost/api/v1/users', {
            method: 'POST',
            body: new FormData(registerForm),
            mode: 'cors',
            headers: acceptJsonHeaders
        });
    };

    const loginRegisteredUser = function(newUser) {
        console.log('user register submit response', newUser);

        currentUser = newUser;
    };

    const createUserError = function(error) {
        console.log('user register submit error', error);
    };

    const loginUserRequest = function() {
        const formData = new FormData(loginForm);
        const email = formData.get('email');
        const password = formData.get('password');

        return new Request('http://localhost/api/v1/users/login?email='+email+'&password='+password, {
            method: 'GET',
            mode: 'cors',
            headers: acceptJsonHeaders
        });
    };

    const loginUser = function(user) {
        console.log('user login submit response', user);

        currentUser = user;

        userInfoContainer.innerHTML = user.first_name + ' ' + user.last_name;

        userSignUpButton.className = "hidden";
        userInfoContainer.className = userInfoContainer.className.replace(/\bhidden\b/, "");
    };

    const loginUserError = function(error) {
        console.log('user login submit error', error);
    };

    const fillUsersContainer = function(users) {
        console.log('Users', users);

        // Clear previous Users
        while (usersContainer.lastChild) {
            usersContainer.removeChild(usersContainer.lastChild);
        }

        // Add all the Users
        users.forEach(user => {
            const userContainer = document.createElement('div');
            userContainer.setAttribute('class', 'blog-user');
            
            const userContent = document.createElement('a');
            userContent.setAttribute('href', '#');
            userContent.innerHTML = user.first_name + ' ' + user.last_name;
            userContainer.appendChild(userContent); 

            usersContainer.appendChild(userContainer);
        });
    };

    const fallbackUsers = function(error) {
        console.log('users error', error);
    };

    fetch(getUsersRequest)
        .then(responseToJson)
        .then(fillUsersContainer)
        .catch(fallbackUsers);

    // User registration Form
    registerForm.addEventListener('submit', function(e) {
        e.preventDefault();

        fetch(createUserRequest())
            .then(responseToJson)
            .then(loginRegisteredUser)
            .catch(createUserError);
    });

    // User login Form
    loginForm.addEventListener('submit', function(e) {
        e.preventDefault();

        fetch(loginUserRequest())
            .then(responseToJson)
            .then(loginUser)
            .catch(loginUserError);
    });
});