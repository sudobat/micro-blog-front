<!DOCTYPE html>
<html>
<head>
    <!-- JQuery T_T -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- My awesome JS -->
    <script src="js/bundle.js"></script>

    <title>MicroBlog v1</title>
</head>
<body>
    <div id="app" style="width:960px; margin-left: auto; margin-right:auto;">
        <header>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">MicroBlog</a>
                    </div>
                    <div class="pull-right">
                        <button type="button" id="blog-user-sign-up-button" class="btn btn-primary navbar-btn" data-toggle="modal" data-target="#blog-sign-up-modal">Sign up</button>
                        <span id="blog-user-info" class="navbar-brand hidden"></span>
                    </div>
                </div>
            </nav>
        </header>

        <div class="container-fluid">
            <div class="row">
                <div id="content" class="col-md-9">

                    <!-- Article -->
                    <article id="blog-article"></article>

                    <!-- Articles pagination -->
                    <nav aria-label="Paginator">
                        <ul class="pager">
                            <li><a href="#" id="blog-paginator-prev">Previous</a></li>
                            <li><a href="#" id="blog-paginator-next">Next</a></li>
                        </ul>
                    </nav>

                    <!-- Comments -->
                    <div id="blog-comments-wrapper">
                        <h2>Comments</h2>

                        <!-- Comments Form -->
                        <form id="blog-comments-form" method="POST">
                            <div class="form-group">
                                <input type="hidden" name="article_id" value="5">
                                <input type="hidden" name="user_id" value="1">
                                <textarea name="content" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Send</button>
                            </div>
                        </form>

                        <!-- Comments List -->
                        <div id="blog-comments"></div>
                    </div>
                </div>
                <div id="sidebar" class="col-md-3">

                    <!-- Categories -->
                    <div id="blog-categories">
                        <h2>Categories</h2>
                        <ul>
                            <li><a href="#">Category 1</a></li>
                            <li>
                                <a href="#">Category 2</a>
                                <ul>
                                    <li><a href="#">Category 2.1</a></li>
                                    <li><a href="#">Category 2.2</a></li>
                                    <li><a href="#">Category 2.3</a></li>
                                    <li><a href="#">Category 2.4</a></li>
                                    <li><a href="#">Category 2.5</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Category 3</a></li>
                            <li><a href="#">Category 4</a></li>
                            <li>
                                <a href="#">Category 5</a>
                                <ul>
                                    <li><a href="#">Category 5.1</a></li>
                                    <li><a href="#">Category 5.2</a></li>
                                    <li><a href="#">Category 5.3</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Category 6</a></li>
                            <li><a href="#">Category 7</a></li>
                        </ul>
                    </div>

                    <!-- Users -->
                    <div id="blog-users-wrapper" class="container-fluid">
                        <h2>Users</h2>

                        <!-- User List -->
                        <div id="blog-users"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- User registration Form -->
    <div id="blog-sign-up-modal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="blog-register-form" method="POST">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Register</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" class="form-control"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="first_name">First Name</label>
                            <input type="text" name="first_name" class="form-control"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="last_name">Last Name</label>
                            <input type="text" name="last_name" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <p class="pull-left">Already registered? <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#blog-sign-in-modal">Sign In</a></p>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Register</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- User sign in Form -->
    <div id="blog-sign-in-modal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="blog-login-form" method="POST">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Sign In</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" class="form-control"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Sign In</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>