<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$client = new \GuzzleHttp\Client();
$consulFactory = new SensioLabs\Consul\ServiceFactory(['base_uri' => 'http://consul_bootstrap:8500']);
$consulCatalog = $consulFactory->get('catalog');

  //////////////
 // Frontend //
//////////////
$app->get('/', function () use ($app) {
    return view('index');
});



  /////////
 // API //
/////////
$app->group([
    'prefix' => 'api/v1',
    'namespace' => 'App\Http\Controllers'
], function ($app) use ($consulCatalog, $client) {
    
    // Articles
    $app->get('articles', 'ArticlesController@getAll');
    $app->get('articles/{id}', 'ArticlesController@getById');

    // Comments
    $app->get('comments', 'CommentsController@getAll');
    $app->post('comments', 'CommentsController@create');

    // Users
    $app->get('users', 'UsersController@getAll');
    $app->get('users/login', 'UsersController@login');
    $app->post('users', 'UsersController@create');

    // Application Seeder
    $app->get('seed', 'SeedController@seed');
});
