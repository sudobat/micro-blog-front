<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->articlesService = $this->getService('articles');
    }

    public function getAll(Request $request)
    {
        if (! $this->healthCheck('articles')) {
            return response()->json();
        }
        
        $articles = json_decode(
            $this->client->get(
                $this->articlesService->ServiceAddress.'/articles'
            )->getBody()->getContents()
        );

        return response()->json($articles);
    }

    public function getById(Request $request, $id)
    {
        if (! $this->healthCheck('articles')) {
            return response()->json();
        }

        $article = json_decode(
            $this->client->get(
                $this->articlesService->ServiceAddress.'/articles/'.$id
            )->getBody()->getContents()
        );

        return response()->json($article);
    }
}
