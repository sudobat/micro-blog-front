<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        if (! $this->healthCheck('users')) {
            return;
        }

        $this->usersService = $this->getService('users');
    }

    public function getAll(Request $request)
    {
        if (! $this->healthCheck('users')) {
            return response()->json();
        }

        $users = json_decode(
            $this->client->get(
                $this->usersService->ServiceAddress.'/users'
            )->getBody()->getContents()
        );

        return response()->json($users);
    }

    public function login(Request $request)
    {
        if (! $this->healthCheck('users')) {
            return response()->json();
        }

        $user = json_decode(
            $this->client->get(
                $this->usersService->ServiceAddress.'/users/login',
                [
                    'query' => [
                        'email'     => $request->email,
                        'password'  => $request->password
                    ]
                ]
            )->getBody()->getContents()
        );

        return response()->json($user);
    }

    public function create(Request $request)
    {
        if (! $this->healthCheck('users')) {
            return response()->json();
        }

        $newUser = json_decode(
            $this->client->post(
                $this->usersService->ServiceAddress.'/users',
                [
                    'form_params' => [
                        'email'         => $request->email,
                        'first_name'    => $request->first_name,
                        'last_name'     => $request->last_name
                    ]
                ]
            )->getBody()->getContents()
        );

        return response()->json($newUser);
    }
}
