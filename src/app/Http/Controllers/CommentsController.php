<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CommentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->commentsService = $this->getService('comments');
    }

    public function getAll(Request $request)
    {
        if (! $this->healthCheck('comments')) {
            return response()->json();
        }

        $comments = json_decode(
            $this->client->get(
                $this->commentsService->ServiceAddress.'/comments'
            )->getBody()->getContents()
        );

        return response()->json($comments);
    }

    public function getByArticleId(Request $request, $articleId)
    {
        if (! $this->healthCheck('comments')) {
            return response()->json();
        }

        $comments = json_decode(
            $this->client->get(
                $this->commentsService->ServiceAddress.'/comments',
                [
                    'query' => [
                        'article_id' => $articleId
                    ]
                ]
            )->getBody()->getContents()
        );

        return response()->json($comments);
    }

    public function create(Request $request)
    {
        if (! $this->healthCheck('comments')) {
            return response()->json();
        }

        $newComment = json_decode(
            $this->client->post(
                $this->commentsService->ServiceAddress.'/comments',
                [
                    'form_params' => [
                        'article_id'    => $request->article_id,
                        'user_id'       => $request->user_id,
                        'content'       => $request->content
                    ]
                ]
            )->getBody()->getContents()
        );

        return response()->json($newComment);
    }
}
