<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

use GuzzleHttp\Client;
use SensioLabs\Consul\ServiceFactory;

class Controller extends BaseController
{
    protected $client;
    protected $consulFactory;
    protected $consulCatalog;

    public function __construct()
    {
        $this->client = new Client();

        $this->consulFactory = new ServiceFactory([
            'base_uri' => 'http://consul_bootstrap:8500'
        ]);

        $this->catalog  = $this->consulFactory->get('catalog');
        $this->health   = $this->consulFactory->get('health');
    }

    protected function getService($serviceName)
    {
        $service = json_decode(
            $this->catalog->service($serviceName.'-80')->getBody()
        );

        if (! $service) {
            return null;
        }

        return $service[0];
    }

    protected function healthCheck($serviceName)
    {
        /*
        return json_decode(
            $this->health->service($serviceName.'-80')->getBody()
        )[0]->Checks[0]->Status == "passing";
        */
        return $this->getService($serviceName) != null;
    }
}
