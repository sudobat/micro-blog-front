<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use SensioLabs\Consul\ServiceFactory;

use Illuminate\Http\Request;

class SeedController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->articlesService  = $this->getService('articles');
        $this->commentsService  = $this->getService('comments');
        $this->usersService     = $this->getService('users');

        $this->faker = \Faker\Factory::create();
    }

    public function seed(Request $request)
    {
        // Seed Articles
        for ($i = 0; $i < 10; $i++) {
            $this->client->post(
                $this->articlesService->ServiceAddress.'/articles',
                [
                    'form_params' => [
                        'title'     => $this->faker->sentence(6),
                        'content'   => $this->faker->paragraph(6),
                    ]
                ]
            );
        }

        // Seed Users
        for ($i = 0; $i < 10; $i++) {
            $this->client->post(
                $this->usersService->ServiceAddress.'/users',
                [
                    'form_params' => [
                        'email'         => $this->faker->email,
                        'first_name'    => $this->faker->firstName,
                        'last_name'     => $this->faker->lastName,
                    ]
                ]
            );
        }

        // Seed Comments
        for ($i = 0; $i < 100; $i++) {
            $this->client->post(
                $this->commentsService->ServiceAddress.'/comments',
                [
                    'form_params' => [
                        'article_id'    => rand(1, 10),
                        'user_id'       => rand(1, 10),
                        'content'       => $this->faker->sentence(10),
                    ]
                ]
            );
        }
    }
}
